import Vue from 'vue'
import VueRouter from 'vue-router'
import Footer from '@/components/footer'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    components: {
      default: () => import('@/views/home/index'),
      footer: Footer
    }
  },

  {
    path: '/kind',
    name: 'kind',
    components: {
      default: () => import('@/views/kind/index'),
      footer: Footer
    }
  },

  {
    path: '/detail/:proid',
    name: 'detail',
    component: () => import('@/views/detail/index')
  },

  {
    path: '/cart',
    name: 'cart',
    components: {
      default: () => import('@/views/cart/index'),
      footer: Footer
    }
  },

  {
    path: '/user',
    name: 'user',
    components: {
      default: () => import('@/views/user/index'),
      footer: Footer
    }
  },

  {
    path: '/search',
    name: 'search',
    component: () => import('@/views/search/index')
  },

  {
    path: '/register',
    name: 'register',
    component: () => import('@/views/register/index')
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index')
  },

  {
    path: '/order/:orderid',
    name: 'order',
    component: () => import('@/views/order/index')
  },

  {
    path: '/adress',
    name: 'adress',
    component: () => import('@/views/adresslist/index')
  },

  {
    path: '/addAdress',
    name: 'addAdress',
    component: () => import('@/components/addAdress')
  },

  {
    path: '/products',
    name: 'products',
    components: {
      default: () => import('@/views/product/index'),
      footer: Footer
    }
  },

  {
    path: '/edituser',
    name: 'edituser',
    component: () => import('@/views/user/edituser.vue')
  },

  {
    path: '/collection',
    name: 'collection',
    component: () => import('@/views/collection/index')
  },

  {
    path: '*',
    name: 'notfound',
    component: () => import('@/views/Notfound/index')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
