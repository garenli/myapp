// 运用axios请求数据的方法
import request from './request'

/**
 * 获取所有产品的数据
 * 数据接口为 http://localhost:3000/apidoc/#api-Pro-GetApiPro
 * @param {Object} params 有limit和count组成的对象
 */
export function getProlist (params) {
  const data = params || { limit: 10, count: 0 }
  return request({
    url: '/pro',
    method: 'get',
    params: data
  })
}

/**
 * 请求banner数据
 */
export function getBannerlist () {
  return request({
    url: '/banner',
    method: 'get'
  })
}

/**
 * 详情数据请求
 */
export function getDetaillist (params) {
  return request({
    url: '/pro/detail',
    method: 'get',
    params
  })
}

// 登录请求
export function login (params) {
  return request({
    url: '/user/login',
    method: 'post',
    data: params
  })
}

// 注册请求
export function register (params) {
  return request({
    url: '/user/register',
    method: 'post',
    data: params
  })
}

// 购物车请求
export function cart (params) {
  return request({
    url: '/cart',
    method: 'get',
    params
  })
}

// 添加购物车请求
export function cartAdd (params) {
  return request({
    url: '/cart/add',
    method: 'get',
    params
  })
}

// 添加购物车更新请求
export function cartUpdate (params) {
  return request({
    url: '/cart/update',
    method: 'get',
    params
  })
}

// 添加购物车删除请求
export function cartDelete (params) {
  return request({
    url: '/cart/delete',
    method: 'get',
    params
  })
}

// 添加购物车选框状态请求
export function cartUpdateflag (params) {
  return request({
    url: '/cart/updateflag',
    method: 'get',
    params
  })
}

// 点击结算生成订单信息请求
export function orderAdd (data) {
  return request({
    url: '/order/add',
    method: 'post',
    data
  })
}

// 订单页面数据请求
export function order (params) {
  return request({
    url: '/order',
    method: 'get',
    params
  })
}

// 地址接口数据请求
export function adress (params) {
  return request({
    url: '/adress',
    method: 'get',
    params
  })
}

// 新增地址接口数据请求
export function addAdress (params) {
  return request({
    url: '/adress/add',
    method: 'get',
    params
  })
}

// 订单地址数据改变请求
export function updateAdress (params) {
  return request({
    url: '/adress/updateAdress',
    method: 'get',
    params
  })
}

// 品牌列表请求
export function typeList () {
  return request({
    url: '/catagory/typeList',
    method: 'get'
  })
}

// 品牌请求
export function typeBrandList (params) {
  return request({
    url: '/catagory/typeListBrand',
    method: 'get',
    params
  })
}

// 品牌下产品请求
export function proList (params) {
  return request({
    url: '/catagory/typeListBrandlist',
    method: 'get',
    params
  })
}

// 用户信息请求
export function userinfo (params) {
  return request({
    url: '/user/userinfo',
    method: 'get',
    params
  })
}

// 用户信息更改请求
export function updateUser (params) {
  return request({
    url: '/user/updateUser',
    method: 'post',
    data: params
  })
}

// 用户头像信息更改请求
export function updateAvatar (params) {
  return request({
    url: '/user/updateAvatar',
    method: 'post',
    data: params
  })
}

// 用户收藏夹数据请求
export function getCollection (params) {
  return request({
    url: '/pro/collection',
    method: 'get',
    params
  })
}
